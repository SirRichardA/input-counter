/*
 * This is a jQuery plugin that places a counter that shows how many characters
 * there should be able to be typed.
 * 
 * For the counter to work, an input should have the data-maxlength attribute
 * 
 * @author Richard Augspurger
 */

(function($)
{
    $.fn.inputCounter = function(options)
    {
		// The settings
		var settings = $.extend(
		{
			counterClasses: "",				// (string) Additional classes
			autoShowHide: true,				// (boolean) Whether or not the counter should automatically hide or not
			curLengthClass: 'curLength',	// (string) Classname for the current length counter
			maxLengthClass: 'maxLength',	// (string) Classname for the max length counter
			maxLength: 0,					// (int) The default max length, 0 or lower means disabled
			defaultColor: '#000000',		// (string) The default color
			halfColor: '#DFDF20',			// (string) The color when half of characters is used
			almostColor: '#DBA901',			// (string) The color when 75% of the characters are used
			fullColor: '#F00000'			// (string) The color when all characters are used
		}, options);
		
		// Create the counter
		function createCounter(curLen, maxLen)
		{
			// Create all nodes
			var $container = $(document.createElement('span')),
				$curLength = $(document.createElement('span')),
				$maxLength = $(document.createElement('span')),
				$splitter = $(document.createTextNode('/'));
				
			// Initialize the counters
			$curLength.text(curLen).addClass(settings.curLengthClass);
			$maxLength.text(maxLen).addClass(settings.maxLengthClass);
			
			// Put everything in place
			$container.append($curLength).append($splitter).append($maxLength);
			
			// Auto show/hide
			if(settings.autoShowHide)
			{
				$container.css('display', 'none');
			}
			
			// Add additional classes if possible
			if(settings.counterClasses != "")
			{
				$container.addClass(settings.counterClasses);
			}
			
			return $container;
		}
	
        return this.each(function()
        {
            // Initialize some stuff
            var self = this,
                $self = $(self),
                curLength = $self.val().length,
                $counter,
			
				// The updater
				updateCounter = function()
				{
					// Get length and default color
					var curLength = $self.val().length,
						color = settings.defaultColor;
					
					// 100% or more of characters used
					if(curLength >= maxLength)
					{
						color = settings.fullColor;
					}
					
					// 75% or more of characters used
					else if(curLength >= count75)
					{
						color = settings.almostColor;
					}
					
					// 50% or more of characters used
					else if(curLength >= count50)
					{
						color = settings.halfColor;
					}
					
					// Set text and color
					$counter.css('color', color).find('.' + settings.curLengthClass).text(curLength);
				},
			
				// The max length
				getMaxLength = function()
				{
					// Try to use data-maxlength first
					if($self.data('maxlength') !== undefined && !isNaN($self.data('maxlength')) && $self.data('maxlength') !== "")
					{
						return $self.data('maxlength');
					}
					
					// data-maxlength is not set, default will be used
					return settings.maxLength;
				},
				// Set max length and calculate 50% and 75%
				maxLength = getMaxLength(),
				count50 = Math.round(maxLength * .5),
				count75 = Math.round(maxLength * .75);

            // Only trigger on valid max lengths
            if(maxLength > 0)
            {
                // Cache the counter and append it
                $counter = createCounter(curLength, maxLength);
				
				// Append the counter
                $self.after($counter);

				// Show how many characters there are typed
                $self
					.keyup(updateCounter)
					.blur(updateCounter)
					.focus(updateCounter);

				// Auto show/hide
				if(settings.autoShowHide)
				{
					$self.blur(function()
					{
						// Hide on blur
						$counter.hide();
					});

					$self.focus(function()
					{
						// Show on focus
						$counter.show();
					});
				}
            }
        });
    };
})(jQuery);